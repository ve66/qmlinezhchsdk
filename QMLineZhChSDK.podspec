

Pod::Spec.new do |spec|


  spec.name         = "QMLineZhChSDK"
  spec.version      = "3.7.1"
  spec.summary      = "A short description of QMLineZhChSDK."

  spec.description  = "QMlineSDK SDK"

  spec.homepage     = " https://VE66@bitbucket.org/ve66/qmlinezhchsdk"

  spec.license      = { :type => "MIT", :file => "FILE_LICENSE" }

  spec.author             = { "ZCZ" => "942914231@qq.com" }

  spec.platform     = :ios

  spec.ios.deployment_target = "9.0"


  spec.source       = { :git => "https://VE66@bitbucket.org/ve66/qmlinezhchsdk.git", :tag => "#{spec.version}" }


  spec.vendored_frameworks  = "QMLineSDK.framework"
  spec.pod_target_xcconfig = {'VALID_ARCHS'=>'armv7 x86_64 arm64'}

  spec.requires_arc = true
  spec.dependency 'Qiniu', '~> 7.2.5'
  spec.dependency 'FMDB', '~> 2.7.5'
  spec.dependency 'SocketRocket', '~> 0.5.1'

end
